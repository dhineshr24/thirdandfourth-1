package com.protoassignments.first;

import com.protoobjects.Attendance;
import com.protoobjects.Building;
import com.protoobjects.Employee;
import org.apache.hadoop.fs.Path;

import java.util.ArrayList;

//Class to load data to proto objects and write from proto objects to HDFS
public class ProtoDataMain {

    private String EMPLOYEE_FILE = System.getProperty("user.dir")+"/output/Employee.csv";
    private String BUILDING_FILE = System.getProperty("user.dir")+"/output/Building.csv";

    public void printProtoObjectsData(){

        Path hdfsAttendanceSeq = new Path("/user/DevHarsha/ProtoSequence/attendance.seq");
        Path hdfsEmployeeSeq = new Path("/user/DevHarsha/ProtoSequence/employee.seq");
        Path hdfsBuildingSeq = new Path("/user/DevHarsha/ProtoSequence/building.seq");
        int NO_OF_DAYS_ATTENDANCE = 10;

        EmployeeProtoData employeeProtoData = new EmployeeProtoData(EMPLOYEE_FILE,hdfsEmployeeSeq);
        BuildingProtoData buildingProtoData = new BuildingProtoData(BUILDING_FILE,hdfsBuildingSeq);
        AttendanceProtoData attendanceProtoData = new AttendanceProtoData(EMPLOYEE_FILE,hdfsAttendanceSeq,NO_OF_DAYS_ATTENDANCE);

        ArrayList<Attendance.Builder> attendance = attendanceProtoData.populateProto();
        attendanceProtoData.writeToSequenceFile();


        ArrayList<Employee.Builder> employees = employeeProtoData.populateProto();
        employeeProtoData.writeToSequenceFile();


        ArrayList<Building.Builder> buildings = buildingProtoData.populateProto();
        buildingProtoData.writeToSequenceFile();

        /*We can printh the Proto Objects data using the data stored in Above Objects list*/

    }

}
