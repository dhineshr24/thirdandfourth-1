package com.protoassignments.second;

import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class BulkLoadEmployeeMapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, Put> {

    private static final byte[] CF_NAME = Bytes.toBytes("information");

    private final byte[][] QUAL_BYTES = {"name".getBytes(), "employee_id".getBytes(),
            "building_code".getBytes(),"floor_number".getBytes(),
            "salary".getBytes(),"department".getBytes()};

    @Override
    protected void map(LongWritable key, Text value,
                       Mapper<LongWritable, Text, ImmutableBytesWritable, Put>.Context context) throws IOException, InterruptedException {

        if (value.getLength() == 0) {
            return;
        }

        //name, employee_id, building_code, floor_number (this should be enum), salary, department
        byte[] rowKey = Bytes.toBytes(String.valueOf(key));
        String data[] = value.toString().split(",");
        Put put = new Put(rowKey);

        put.addColumn(CF_NAME, QUAL_BYTES[0], Bytes.toBytes(data[0].substring(1,data[0].length()-1)));
        put.addColumn(CF_NAME, QUAL_BYTES[1], Bytes.toBytes(data[1].substring(1,data[1].length()-1)));
        put.addColumn(CF_NAME, QUAL_BYTES[2], Bytes.toBytes(data[2].substring(1,data[2].length()-1)));
        put.addColumn(CF_NAME, QUAL_BYTES[3], Bytes.toBytes(data[3].substring(1,data[3].length()-1)));
        put.addColumn(CF_NAME, QUAL_BYTES[4], Bytes.toBytes(data[4].substring(1,data[4].length()-1)));
        put.addColumn(CF_NAME, QUAL_BYTES[5], Bytes.toBytes(data[5].substring(1,data[5].length()-1)));

        context.write(new ImmutableBytesWritable(rowKey), put);

    }
}


