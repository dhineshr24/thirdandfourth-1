package com.protoassignments.third;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeCafeteriaDriver extends Configured implements Tool {

    private String BUILDING_TABLE;
    private String EMPLOYEE_TABLE;
    private Path OUTPUT;

    public EmployeeCafeteriaDriver(Path hdfsPath,String employeeTable,String buildingTable){
        this.BUILDING_TABLE = buildingTable;
        this.EMPLOYEE_TABLE = employeeTable;
        this.OUTPUT = hdfsPath;
    }

    @Override
    public int run(String[] args) {
        List<Scan> scans = new ArrayList<>();
        //adding table name to scan list
        scans.add(scan(BUILDING_TABLE));
        scans.add(scan(EMPLOYEE_TABLE));
        HBaseConfiguration conf = new HBaseConfiguration();
        Job job = getJob(scans, conf);
        return 0;
    }

    private Job getJob(List<Scan> scans, HBaseConfiguration conf) {

        Job job = null;
        try {
            job = new Job(conf, "Populating employees Cafeteria code");
            job.setJarByClass(EmployeeCafeteriaDriver.class);
            job.setNumReduceTasks(0);
            FileOutputFormat.setOutputPath(job, OUTPUT);
            job.setOutputFormatClass(SequenceFileOutputFormat.class);
            job.setOutputKeyClass(LongWritable.class);
            job.setOutputValueClass(ImmutableBytesWritable.class);
            TableMapReduceUtil.initTableMapperJob(scans,
                    EmployeeCafeteriaMapper.class, LongWritable.class,
                    ImmutableBytesWritable.class, job);

            job.waitForCompletion(true);
        } catch (IOException | InterruptedException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return job;
    }

    public Scan scan(String tableName)
    {
        Scan scan = new Scan();
        scan.setAttribute("scan.attributes.table.name", Bytes.toBytes(tableName));
        return scan;
    }


}
