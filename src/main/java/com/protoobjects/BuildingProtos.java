// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Building.proto

package com.protoobjects;

public final class BuildingProtos {
  private BuildingProtos() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_third_assignment_Building_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_third_assignment_Building_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\016Building.proto\022\020third_assignment\"r\n\010Bu" +
      "ilding\022\025\n\rbuilding_code\030\001 \001(\005\022\024\n\014total_f" +
      "loors\030\002 \001(\005\022!\n\031companies_in_the_building" +
      "\030\003 \001(\005\022\026\n\016cafeteria_code\030\004 \001(\005B&\n\022com.ob" +
      "jects.protosB\016BuildingProtosP\001"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_third_assignment_Building_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_third_assignment_Building_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_third_assignment_Building_descriptor,
        new java.lang.String[] { "BuildingCode", "TotalFloors", "CompaniesInTheBuilding", "CafeteriaCode", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
